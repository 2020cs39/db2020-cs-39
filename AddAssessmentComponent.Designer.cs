﻿
namespace DB2020_CS_39
{
    partial class AddAssessmentComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.nameTBox = new System.Windows.Forms.TextBox();
            this.assessmentTitle = new System.Windows.Forms.Label();
            this.totalMarksTBox = new System.Windows.Forms.TextBox();
            this.assessmentTotalMarks = new System.Windows.Forms.Label();
            this.assessmentTotalWeightage = new System.Windows.Forms.Label();
            this.addAssessmentComponentBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rubricIDTBox = new System.Windows.Forms.ComboBox();
            this.AssessmentIDTBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26F));
            this.tableLayoutPanel1.Controls.Add(this.nameTBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.assessmentTitle, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.totalMarksTBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.assessmentTotalMarks, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.assessmentTotalWeightage, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.addAssessmentComponentBtn, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.rubricIDTBox, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.AssessmentIDTBox, 4, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(80, 133);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(551, 213);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // nameTBox
            // 
            this.nameTBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTBox.Location = new System.Drawing.Point(91, 24);
            this.nameTBox.Name = "nameTBox";
            this.nameTBox.Size = new System.Drawing.Size(137, 23);
            this.nameTBox.TabIndex = 11;
            // 
            // assessmentTitle
            // 
            this.assessmentTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.assessmentTitle.AutoSize = true;
            this.assessmentTitle.Location = new System.Drawing.Point(3, 27);
            this.assessmentTitle.Name = "assessmentTitle";
            this.assessmentTitle.Size = new System.Drawing.Size(82, 16);
            this.assessmentTitle.TabIndex = 8;
            this.assessmentTitle.Text = "Name";
            // 
            // totalMarksTBox
            // 
            this.totalMarksTBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.totalMarksTBox.Location = new System.Drawing.Point(91, 95);
            this.totalMarksTBox.Name = "totalMarksTBox";
            this.totalMarksTBox.Size = new System.Drawing.Size(137, 23);
            this.totalMarksTBox.TabIndex = 10;
            // 
            // assessmentTotalMarks
            // 
            this.assessmentTotalMarks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.assessmentTotalMarks.AutoSize = true;
            this.assessmentTotalMarks.Location = new System.Drawing.Point(3, 90);
            this.assessmentTotalMarks.Name = "assessmentTotalMarks";
            this.assessmentTotalMarks.Size = new System.Drawing.Size(82, 32);
            this.assessmentTotalMarks.TabIndex = 4;
            this.assessmentTotalMarks.Text = "Total Marks";
            // 
            // assessmentTotalWeightage
            // 
            this.assessmentTotalWeightage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.assessmentTotalWeightage.AutoSize = true;
            this.assessmentTotalWeightage.Location = new System.Drawing.Point(322, 11);
            this.assessmentTotalWeightage.Name = "assessmentTotalWeightage";
            this.assessmentTotalWeightage.Size = new System.Drawing.Size(82, 48);
            this.assessmentTotalWeightage.TabIndex = 7;
            this.assessmentTotalWeightage.Text = "Select Assessment ";
            // 
            // addAssessmentComponentBtn
            // 
            this.addAssessmentComponentBtn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.addAssessmentComponentBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.addAssessmentComponentBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.addAssessmentComponentBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addAssessmentComponentBtn.Location = new System.Drawing.Point(3, 157);
            this.addAssessmentComponentBtn.Name = "addAssessmentComponentBtn";
            this.addAssessmentComponentBtn.Size = new System.Drawing.Size(82, 41);
            this.addAssessmentComponentBtn.TabIndex = 16;
            this.addAssessmentComponentBtn.Text = "ADD";
            this.addAssessmentComponentBtn.UseVisualStyleBackColor = false;
            this.addAssessmentComponentBtn.Click += new System.EventHandler(this.addAssessmentComponentBtn_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(322, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 32);
            this.label2.TabIndex = 17;
            this.label2.Text = "Select Rubric";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // rubricIDTBox
            // 
            this.rubricIDTBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rubricIDTBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rubricIDTBox.FormattingEnabled = true;
            this.rubricIDTBox.Location = new System.Drawing.Point(410, 95);
            this.rubricIDTBox.Name = "rubricIDTBox";
            this.rubricIDTBox.Size = new System.Drawing.Size(138, 24);
            this.rubricIDTBox.TabIndex = 19;
            // 
            // AssessmentIDTBox
            // 
            this.AssessmentIDTBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.AssessmentIDTBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AssessmentIDTBox.FormattingEnabled = true;
            this.AssessmentIDTBox.Location = new System.Drawing.Point(410, 23);
            this.AssessmentIDTBox.Name = "AssessmentIDTBox";
            this.AssessmentIDTBox.Size = new System.Drawing.Size(138, 24);
            this.AssessmentIDTBox.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(166, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(334, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add Assessment Component";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.96688F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.85501F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.1781F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(16, 12);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(656, 55);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // AddAssessmentComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 475);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddAssessmentComponent";
            this.Text = "AddAssessmentComponent";
            this.Load += new System.EventHandler(this.AddAssessmentComponent_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox nameTBox;
        private System.Windows.Forms.Label assessmentTitle;
        private System.Windows.Forms.TextBox totalMarksTBox;
        private System.Windows.Forms.Label assessmentTotalMarks;
        private System.Windows.Forms.Label assessmentTotalWeightage;
        private System.Windows.Forms.Button addAssessmentComponentBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox rubricIDTBox;
        private System.Windows.Forms.ComboBox AssessmentIDTBox;
    }
}