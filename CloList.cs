﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class CloList : Form
    {
        public CloList()
        {
            InitializeComponent();
        }
        private Form1 f1 = new Form1();
        private string queryClo = "Select * From Clo";
        private void deleteBtn_Click(object sender, EventArgs e)
        {

        }

        private void CloList_Load(object sender, EventArgs e)
        {
            f1.loaDdataIntoGrid(cloDataGrid, queryClo);
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime systemDate = DateTime.Now;
                DateTime currentDate = systemDate.Date;
                string selected_id = cloDataGrid.CurrentRow.Cells[0].Value.ToString();
                string selected_Name = cloDataGrid.CurrentRow.Cells[1].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE Clo SET Name =  @Name, DateUpdated = @DateUpdated WHERE Id = '" + selected_id + "' ", con);
                cmd.Parameters.AddWithValue("@Name", selected_Name);
                cmd.Parameters.AddWithValue("@DateUpdated", currentDate);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(cloDataGrid, queryClo);
            }

        }
    }
}
