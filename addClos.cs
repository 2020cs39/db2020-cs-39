﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class addClos : Form
    {
        public addClos()
        {
            InitializeComponent();
        }

        private void addStdBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string cloName = cloNameTBox.Text;
                DateTime systemDate = DateTime.Now;
                DateTime currentDate = systemDate.Date;
                string sysFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Clo values  (@Name, @DateCreated , @DateUpdated)", con);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@DateCreated", currentDate);
                cmd.Parameters.AddWithValue("@DateUpdated", currentDate);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                cloNameTBox.Text = " ";
            }

        }
    }
}
