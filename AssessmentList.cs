﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class assessmentList : Form
    {
        public assessmentList()
        {
            InitializeComponent();
        }
        private Form1 f1 = new Form1();
        private string queryForAssessment = "Select Title, TotalMarks As [Total Marks], " +
                "TotalWeightage As [Total Weightage] , DateCreated As [Date Created] From Assessment";

        private void assessmentList_Load(object sender, EventArgs e)
        {
            f1.loaDdataIntoGrid(assesssmentDataGrid, queryForAssessment);
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int selected_Id = 0;
                string forConversion = " ";
                List<int> acId = new List<int>();
                string selected_title = assesssmentDataGrid.CurrentRow.Cells[0].Value.ToString();
                string findStdQuery = "Select Id From Assessment Where Title Like '" + selected_title + "' ";
                var conRubric = Configuration.getInstance().getConnection();
                SqlCommand findRubricID = new SqlCommand(findStdQuery, conRubric);
                SqlDataReader RIDreader = findRubricID.ExecuteReader();
                if (RIDreader.HasRows)
                {
                    while (RIDreader.Read())
                    {
                        forConversion = RIDreader["Id"].ToString();
                        selected_Id = int.Parse(forConversion);
                    }
                }
                RIDreader.Close();
                string findAciDQuery = "Select Id From AssessmentComponent Where AssessmentId  Like '" + selected_Id + "' ";
                var conAc = Configuration.getInstance().getConnection();
                SqlCommand findAcID = new SqlCommand(findAciDQuery, conAc);
                SqlDataReader RacIdreader = findAcID.ExecuteReader();
                if (RacIdreader.HasRows)
                {
                    while (RacIdreader.Read())
                    {
                        forConversion = RacIdreader["Id"].ToString();
                        selected_Id = int.Parse(forConversion);
                        acId.Add(selected_Id);
                    }
                }
                RacIdreader.Close();
                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand(("DELETE FROM Assessment WHERE Id = '" + selected_Id + "' ") + ("DELETE FROM AssessmentComponent WHERE AssessmentId = '" + selected_Id + "' ") , con1);
                cmd1.ExecuteNonQuery();
                foreach(var i in acId)
                {
                    var con2 = Configuration.getInstance().getConnection();
                    SqlCommand cmd2 = new SqlCommand(("DELETE FROM StudentResult WHERE AssessmentComponentId = '" + i + "' "), con2);
                    cmd2.ExecuteNonQuery();
                }
                MessageBox.Show("Successfully Deleted");

            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(assesssmentDataGrid, queryForAssessment); // This function call will refresh grid
            }
        }
        private bool isValidMarks(string n)
        {
            bool isValid = false;
            Regex checkName = new Regex(@"^([0-9]+)$");
            isValid = checkName.IsMatch(n);
            return isValid;
        }
        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool allAttributeValid = true;
                int selected_Id = 0;
                string forConversion = " ";
                string selected_title = assesssmentDataGrid.CurrentRow.Cells[0].Value.ToString();
                string selected_tMarks = assesssmentDataGrid.CurrentRow.Cells[1].Value.ToString();
                string selected_tWeightage = assesssmentDataGrid.CurrentRow.Cells[2].Value.ToString();

                string findStdQuery = "Select Id From Assessment Where Title Like '" + selected_title + "' ";
                var conRubric = Configuration.getInstance().getConnection();
                SqlCommand findRubricID = new SqlCommand(findStdQuery, conRubric);
                SqlDataReader RIDreader = findRubricID.ExecuteReader();
                if (RIDreader.HasRows)
                {
                    while (RIDreader.Read())
                    {
                        forConversion = RIDreader["Id"].ToString();
                        selected_Id = int.Parse(forConversion);
                    }
                }
                RIDreader.Close();
                /*
                if(!isValidMarks(selected_tMarks))
                {
                    allAttributeValid = false;
                    MessageBox.Show("Total Marks Should be in Integres");
                }
                if (!isValidMarks(selected_tWeightage))
                {
                    allAttributeValid = false;
                    MessageBox.Show("Total Weightage Should be in Integres");
                }
                */
                if (allAttributeValid)
                {
                    var con1 = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("UPDATE Assessment SET Title = @Title, TotalMarks = @TotalMarks, TotalWeightage = @TotalWeightage Where Id = '" + selected_Id + "' ", con1);
                    cmd.Parameters.AddWithValue("@Title", selected_title);
                    cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(selected_tMarks));
                    cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse(selected_tWeightage));
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Updated");
                }
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(assesssmentDataGrid, queryForAssessment);
            }
        }
    }
}
