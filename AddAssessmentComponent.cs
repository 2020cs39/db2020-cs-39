﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class AddAssessmentComponent : Form
    {
        public AddAssessmentComponent()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private Dictionary<string, string> Ass = new Dictionary<string, string>();
        private Dictionary<string, string> Rubr = new Dictionary<string, string>();

        private void addAssessmentComponentBtn_Click(object sender, EventArgs e)
        {
            try
            {

                int rubricId = 0;
                int assessmentId = 0;
                string name = nameTBox.Text;
                DateTime systemDate = DateTime.Now;
                DateTime currentDate = systemDate.Date;
                int totalMarks = int.Parse(totalMarksTBox.Text);
                string rubricSelect = rubricIDTBox.Text;
                string assessmentSelect = AssessmentIDTBox.Text;

                foreach (var a in Ass)
                {
                    if(a.Value == assessmentSelect)
                    {
                        assessmentId = int.Parse(a.Key);
                    }
                }

                foreach (var r in Rubr)
                {
                    if (r.Value == rubricSelect)
                    {
                        rubricId = int.Parse(r.Key);
                    }
                }

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values  (@Name, @RubricId, @TotalMarks, @DateCreated, @DateUpdated, @AssessmentId)", con);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@RubricId", rubricId);
                cmd.Parameters.AddWithValue("@TotalMarks", totalMarks);
                cmd.Parameters.AddWithValue("@DateCreated", currentDate);
                cmd.Parameters.AddWithValue("@DateUpdated", currentDate);
                cmd.Parameters.AddWithValue("@AssessmentId", assessmentId);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                this.Close();
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                nameTBox.Text = " ";
                rubricIDTBox.Text = " ";
                totalMarksTBox.Text = " ";
                AssessmentIDTBox.Text = " ";
            }
            
        }

        private void AddAssessmentComponent_Load(object sender, EventArgs e)
        {
            try
            {
                var conA = Configuration.getInstance().getConnection();
                SqlCommand findAssessmentID = new SqlCommand("Select Id,Title From Assessment", conA);
                SqlDataReader AIDreader = findAssessmentID.ExecuteReader();
                if (AIDreader.HasRows)
                {
                    while (AIDreader.Read())
                    {
                        Ass.Add(AIDreader["Id"].ToString(), AIDreader["Title"].ToString());
                        AssessmentIDTBox.Items.Add(AIDreader["Title"].ToString());
                    }
                }
                AIDreader.Close();
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

            try
            {
                var conR = Configuration.getInstance().getConnection();
                SqlCommand findRubricID = new SqlCommand("Select Id,Details From Rubric", conR);
                SqlDataReader RIDreader = findRubricID.ExecuteReader();
                if (RIDreader.HasRows)
                {
                    while (RIDreader.Read())
                    {
                        Rubr.Add(RIDreader["Id"].ToString(), RIDreader["Details"].ToString());
                        rubricIDTBox.Items.Add(RIDreader["Details"].ToString());
                    }
                }
                RIDreader.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

        }
    }
}
