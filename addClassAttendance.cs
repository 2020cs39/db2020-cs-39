﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class addClassAttendance : Form
    {
        public addClassAttendance()
        {
            InitializeComponent();
        }

        private void addCAttendanceBtn_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime date = Convert.ToDateTime(getDate.Text);
                DateTime onlyDate = date.Date;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into ClassAttendance values  (@Attendancedate)", con);
                cmd.Parameters.AddWithValue("@Attendancedate", onlyDate);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
    }
}
