﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class rubricLevelList : Form
    {
        public rubricLevelList()
        {
            InitializeComponent();
        }
        private Form1 f1 = new Form1();
        private string queryForRubricLevel = "Select Id, Details, MeasurementLevel As [Measurement Level] From RubricLevel";
        private void rubricLevelList_Load(object sender, EventArgs e)
        {
            f1.loaDdataIntoGrid(rubricLevelDataGrid, queryForRubricLevel);
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string selected_Id = rubricLevelDataGrid.CurrentRow.Cells[0].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(("DELETE FROM RubricLevel WHERE Id = '" + selected_Id + "' ")+ ("DELETE FROM StudentResult WHERE RubricMeasurementId  = '" + selected_Id + "' "), con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(rubricLevelDataGrid, queryForRubricLevel); // This function call will refresh grid
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {

        }

        private void updateBtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                string selected_Id = rubricLevelDataGrid.CurrentRow.Cells[0].Value.ToString();
                string selected_Detail = rubricLevelDataGrid.CurrentRow.Cells[1].Value.ToString();
                string selected_mLevel = rubricLevelDataGrid.CurrentRow.Cells[2].Value.ToString();
                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE RubricLevel SET Details = @Details, MeasurementLevel = @MeasurementLevel Where Id = '" + selected_Id + "' ", con1);
                cmd.Parameters.AddWithValue("@Details", selected_Detail);
                cmd.Parameters.AddWithValue("@MeasurementLevel", int.Parse(selected_mLevel));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(rubricLevelDataGrid, queryForRubricLevel); // This function call will refresh grid
            }
        }
    }
}
