﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace DB2020_CS_39
{
    public partial class addRubrics : Form
    {
        public addRubrics()
        {
            InitializeComponent();
        }
        private Dictionary<string, string> cloIDs = new Dictionary<string, string>();

        private void addRubricBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int clid = 0;
                int rId = int.Parse(rubricIDTBox.Text);
                string detail = rubricDetailTBox.Text;
                string cloIDSelect = cloIDTBox.Text;
                foreach (var a in cloIDs)
                {
                    if (a.Value == cloIDSelect)
                    {
                        clid = int.Parse(a.Key);
                    }
                }
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Rubric values  (@Id, @Details, @CloId)", con);
                cmd.Parameters.AddWithValue("@Id", rId);
                cmd.Parameters.AddWithValue("@Details", detail);
                cmd.Parameters.AddWithValue("@CloId", clid);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                this.Close();
            }
            catch(System.Data.SqlClient.SqlException exp)
            {
                MessageBox.Show("Duplicate key can not be insert");
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                rubricIDTBox.Text = " ";
                rubricDetailTBox.Text = " ";
                cloIDTBox.Text = " ";
            }
            
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
        private void addRubrics_Load(object sender, EventArgs e)
        {
            try
            {
                var conf = Configuration.getInstance().getConnection();
                SqlCommand findCLoID = new SqlCommand("Select Id,Name From Clo", conf);
                SqlDataReader reader = findCLoID.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        cloIDs.Add(reader["Id"].ToString(), reader["Name"].ToString());
                        cloIDTBox.Items.Add(reader["Id"].ToString());
                    }
                }
                reader.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

        }
    }
}
