﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DB2020_CS_39
{
    public partial class addStudentFrom : Form
    {
        public addStudentFrom()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void regNoTBox_TextChanged(object sender, EventArgs e)
        {

        }

        private bool isValidName(string n)
        {
            bool isValid = false;
            Regex checkName = new Regex(@"^([A-Z][a-z-A-Z]+)$");
            isValid = checkName.IsMatch(n);
            return isValid;
        }
        private bool isValidMail(string e)
        {
            bool isValidMail = false;
            Regex checkEmail = new Regex(@"^([w.-]+)@ ([w-]+) ((. (w) {2,3})+)$");
            isValidMail = checkEmail.IsMatch(e);
            return isValidMail;
        }
        private bool isValidRegNo(string r)
        {
            bool isValidReg = false;
            Regex checkReg = new Regex(@"[20][1-2][0-9][-][C][S][-][0-9]{2}$");
            isValidReg = checkReg.IsMatch(r);
            return isValidReg;
        }
        private bool isValidContact(string c)
        {
            bool isValidContact = false;
            Regex checkCon = new Regex(@"^([03][0-9]{9})$");
            isValidContact = checkCon.IsMatch(c);
            return isValidContact;
        }
        private void addStdBtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool allTextBoxValid = true;
                int status = 0;
                string fName = fNameTBox.Text;
                string lName = lNameTBox.Text;
                string contac = conTBox.Text;
                string email = emailTBox.Text;
                string regNo = regNoTBox.Text;
                if (statusTBox.Text == "Active")
                {
                    status = 5;
                }
                else if (statusTBox.Text == "In Active")
                {
                    status = 6;
                }

                if (!isValidName(fName))
                {
                    MessageBox.Show("First Name is not in correct Format");
                    allTextBoxValid = false;
                }
                if (!isValidName(lName))
                {
                    MessageBox.Show("Last Name is not in correct Format");
                    allTextBoxValid = false;
                }
                if (!isValidRegNo(regNo))
                {
                    MessageBox.Show("Registration Number is not in correct format");
                    allTextBoxValid = false;
                }
                if (!isValidContact(contac))
                {
                    MessageBox.Show("Contact is not in correct format");
                    allTextBoxValid = false;
                }
                if (allTextBoxValid)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into Student values  (@FirstName, @LastName , @Contact, @Email, @RegistrationNumber, @Status)", con);
                    cmd.Parameters.AddWithValue("@FirstName", fName);
                    cmd.Parameters.AddWithValue("@LastName", lName);
                    cmd.Parameters.AddWithValue("@Contact", contac);
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", regNo);
                    cmd.Parameters.AddWithValue("@Status", status);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                    this.Close();
                }

            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                fNameTBox.Text = " ";
                lNameTBox.Text = " ";
                conTBox.Text = " ";
                emailTBox.Text = " ";
                regNoTBox.Text = " ";
                statusTBox.Text = " ";
            }


        }


        private void addStudentFrom_Load(object sender, EventArgs e)
        {
            statusTBox.Items.Add("Active");
            statusTBox.Items.Add("In Active");
        }
    }
}
