﻿
namespace DB2020_CS_39
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button Student;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.leftSidePanel = new System.Windows.Forms.Panel();
            this.manageAttendancepanel = new System.Windows.Forms.Panel();
            this.attendanceListBtn = new System.Windows.Forms.Button();
            this.addStudentAttendanceBtn = new System.Windows.Forms.Button();
            this.addDateBtn = new System.Windows.Forms.Button();
            this.StudentAttendance = new System.Windows.Forms.Button();
            this.evaluationPanel = new System.Windows.Forms.Panel();
            this.evaluateStudentListBtn = new System.Windows.Forms.Button();
            this.evaluateStudentBtn = new System.Windows.Forms.Button();
            this.Evaluation = new System.Windows.Forms.Button();
            this.assessmentPanel = new System.Windows.Forms.Panel();
            this.assessmentComponentBtn = new System.Windows.Forms.Button();
            this.assessmentListBtn = new System.Windows.Forms.Button();
            this.addAssessmentComponent = new System.Windows.Forms.Button();
            this.addAssessment = new System.Windows.Forms.Button();
            this.Assessments = new System.Windows.Forms.Button();
            this.rubricsPanel = new System.Windows.Forms.Panel();
            this.rubricLevelListBtn = new System.Windows.Forms.Button();
            this.addRubricLevel = new System.Windows.Forms.Button();
            this.rubricListBtn = new System.Windows.Forms.Button();
            this.addRubricbtn = new System.Windows.Forms.Button();
            this.rubrics = new System.Windows.Forms.Button();
            this.courseSubPanel = new System.Windows.Forms.Panel();
            this.cloListBtn = new System.Windows.Forms.Button();
            this.addCloBtn = new System.Windows.Forms.Button();
            this.CLOs = new System.Windows.Forms.Button();
            this.subPanelStudent = new System.Windows.Forms.Panel();
            this.stdbutton2 = new System.Windows.Forms.Button();
            this.stdbutton1 = new System.Windows.Forms.Button();
            this.upperLeftPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.upperCoverPanel = new System.Windows.Forms.Panel();
            this.belowPanel = new System.Windows.Forms.Panel();
            this.childFormPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            Student = new System.Windows.Forms.Button();
            this.leftSidePanel.SuspendLayout();
            this.manageAttendancepanel.SuspendLayout();
            this.evaluationPanel.SuspendLayout();
            this.assessmentPanel.SuspendLayout();
            this.rubricsPanel.SuspendLayout();
            this.courseSubPanel.SuspendLayout();
            this.subPanelStudent.SuspendLayout();
            this.upperLeftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.childFormPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Student
            // 
            Student.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            Student.Dock = System.Windows.Forms.DockStyle.Top;
            Student.FlatAppearance.BorderSize = 0;
            Student.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            Student.ForeColor = System.Drawing.SystemColors.Control;
            Student.Location = new System.Drawing.Point(0, 135);
            Student.Name = "Student";
            Student.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            Student.Size = new System.Drawing.Size(183, 30);
            Student.TabIndex = 2;
            Student.Text = "Manage Student";
            Student.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            Student.UseVisualStyleBackColor = false;
            Student.Click += new System.EventHandler(this.Student_Click);
            // 
            // leftSidePanel
            // 
            this.leftSidePanel.AutoScroll = true;
            this.leftSidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.leftSidePanel.Controls.Add(this.manageAttendancepanel);
            this.leftSidePanel.Controls.Add(this.StudentAttendance);
            this.leftSidePanel.Controls.Add(this.evaluationPanel);
            this.leftSidePanel.Controls.Add(this.Evaluation);
            this.leftSidePanel.Controls.Add(this.assessmentPanel);
            this.leftSidePanel.Controls.Add(this.Assessments);
            this.leftSidePanel.Controls.Add(this.rubricsPanel);
            this.leftSidePanel.Controls.Add(this.rubrics);
            this.leftSidePanel.Controls.Add(this.courseSubPanel);
            this.leftSidePanel.Controls.Add(this.CLOs);
            this.leftSidePanel.Controls.Add(this.subPanelStudent);
            this.leftSidePanel.Controls.Add(Student);
            this.leftSidePanel.Controls.Add(this.upperLeftPanel);
            this.leftSidePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftSidePanel.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.leftSidePanel.Location = new System.Drawing.Point(0, 0);
            this.leftSidePanel.Name = "leftSidePanel";
            this.leftSidePanel.Size = new System.Drawing.Size(200, 541);
            this.leftSidePanel.TabIndex = 0;
            // 
            // manageAttendancepanel
            // 
            this.manageAttendancepanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.manageAttendancepanel.Controls.Add(this.attendanceListBtn);
            this.manageAttendancepanel.Controls.Add(this.addStudentAttendanceBtn);
            this.manageAttendancepanel.Controls.Add(this.addDateBtn);
            this.manageAttendancepanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.manageAttendancepanel.Location = new System.Drawing.Point(0, 780);
            this.manageAttendancepanel.Name = "manageAttendancepanel";
            this.manageAttendancepanel.Size = new System.Drawing.Size(183, 139);
            this.manageAttendancepanel.TabIndex = 13;
            // 
            // attendanceListBtn
            // 
            this.attendanceListBtn.BackColor = System.Drawing.Color.Gray;
            this.attendanceListBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.attendanceListBtn.FlatAppearance.BorderSize = 0;
            this.attendanceListBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.attendanceListBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.attendanceListBtn.Location = new System.Drawing.Point(0, 66);
            this.attendanceListBtn.Name = "attendanceListBtn";
            this.attendanceListBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.attendanceListBtn.Size = new System.Drawing.Size(183, 33);
            this.attendanceListBtn.TabIndex = 4;
            this.attendanceListBtn.Text = "Attendance List";
            this.attendanceListBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.attendanceListBtn.UseVisualStyleBackColor = false;
            this.attendanceListBtn.Click += new System.EventHandler(this.attendanceListBtn_Click);
            // 
            // addStudentAttendanceBtn
            // 
            this.addStudentAttendanceBtn.BackColor = System.Drawing.Color.Gray;
            this.addStudentAttendanceBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.addStudentAttendanceBtn.FlatAppearance.BorderSize = 0;
            this.addStudentAttendanceBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addStudentAttendanceBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addStudentAttendanceBtn.Location = new System.Drawing.Point(0, 33);
            this.addStudentAttendanceBtn.Name = "addStudentAttendanceBtn";
            this.addStudentAttendanceBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.addStudentAttendanceBtn.Size = new System.Drawing.Size(183, 33);
            this.addStudentAttendanceBtn.TabIndex = 3;
            this.addStudentAttendanceBtn.Text = "Student Attendance";
            this.addStudentAttendanceBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addStudentAttendanceBtn.UseVisualStyleBackColor = false;
            this.addStudentAttendanceBtn.Click += new System.EventHandler(this.addStudentAttendanceBtn_Click);
            // 
            // addDateBtn
            // 
            this.addDateBtn.BackColor = System.Drawing.Color.Gray;
            this.addDateBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.addDateBtn.FlatAppearance.BorderSize = 0;
            this.addDateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addDateBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addDateBtn.Location = new System.Drawing.Point(0, 0);
            this.addDateBtn.Name = "addDateBtn";
            this.addDateBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.addDateBtn.Size = new System.Drawing.Size(183, 33);
            this.addDateBtn.TabIndex = 2;
            this.addDateBtn.Text = "Add Date";
            this.addDateBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addDateBtn.UseVisualStyleBackColor = false;
            this.addDateBtn.Click += new System.EventHandler(this.addDateBtn_Click);
            // 
            // StudentAttendance
            // 
            this.StudentAttendance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.StudentAttendance.Dock = System.Windows.Forms.DockStyle.Top;
            this.StudentAttendance.FlatAppearance.BorderSize = 0;
            this.StudentAttendance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StudentAttendance.ForeColor = System.Drawing.SystemColors.Control;
            this.StudentAttendance.Location = new System.Drawing.Point(0, 750);
            this.StudentAttendance.Name = "StudentAttendance";
            this.StudentAttendance.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.StudentAttendance.Size = new System.Drawing.Size(183, 30);
            this.StudentAttendance.TabIndex = 12;
            this.StudentAttendance.Text = "Manage Attendance";
            this.StudentAttendance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StudentAttendance.UseVisualStyleBackColor = false;
            this.StudentAttendance.Click += new System.EventHandler(this.StudentAttendance_Click);
            // 
            // evaluationPanel
            // 
            this.evaluationPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.evaluationPanel.Controls.Add(this.evaluateStudentListBtn);
            this.evaluationPanel.Controls.Add(this.evaluateStudentBtn);
            this.evaluationPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.evaluationPanel.Location = new System.Drawing.Point(0, 681);
            this.evaluationPanel.Name = "evaluationPanel";
            this.evaluationPanel.Size = new System.Drawing.Size(183, 69);
            this.evaluationPanel.TabIndex = 11;
            // 
            // evaluateStudentListBtn
            // 
            this.evaluateStudentListBtn.BackColor = System.Drawing.Color.Gray;
            this.evaluateStudentListBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.evaluateStudentListBtn.FlatAppearance.BorderSize = 0;
            this.evaluateStudentListBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.evaluateStudentListBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.evaluateStudentListBtn.Location = new System.Drawing.Point(0, 33);
            this.evaluateStudentListBtn.Name = "evaluateStudentListBtn";
            this.evaluateStudentListBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.evaluateStudentListBtn.Size = new System.Drawing.Size(183, 33);
            this.evaluateStudentListBtn.TabIndex = 3;
            this.evaluateStudentListBtn.Text = "Evaluate List";
            this.evaluateStudentListBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.evaluateStudentListBtn.UseVisualStyleBackColor = false;
            this.evaluateStudentListBtn.Click += new System.EventHandler(this.evaluateStudentListBtn_Click);
            // 
            // evaluateStudentBtn
            // 
            this.evaluateStudentBtn.BackColor = System.Drawing.Color.Gray;
            this.evaluateStudentBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.evaluateStudentBtn.FlatAppearance.BorderSize = 0;
            this.evaluateStudentBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.evaluateStudentBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.evaluateStudentBtn.Location = new System.Drawing.Point(0, 0);
            this.evaluateStudentBtn.Name = "evaluateStudentBtn";
            this.evaluateStudentBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.evaluateStudentBtn.Size = new System.Drawing.Size(183, 33);
            this.evaluateStudentBtn.TabIndex = 2;
            this.evaluateStudentBtn.Text = "Evaluate Student";
            this.evaluateStudentBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.evaluateStudentBtn.UseVisualStyleBackColor = false;
            this.evaluateStudentBtn.Click += new System.EventHandler(this.evaluateStudentBtn_Click_1);
            // 
            // Evaluation
            // 
            this.Evaluation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.Evaluation.Dock = System.Windows.Forms.DockStyle.Top;
            this.Evaluation.FlatAppearance.BorderSize = 0;
            this.Evaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Evaluation.ForeColor = System.Drawing.SystemColors.Control;
            this.Evaluation.Location = new System.Drawing.Point(0, 651);
            this.Evaluation.Name = "Evaluation";
            this.Evaluation.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.Evaluation.Size = new System.Drawing.Size(183, 30);
            this.Evaluation.TabIndex = 10;
            this.Evaluation.Text = "Manage Evaluations";
            this.Evaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Evaluation.UseVisualStyleBackColor = false;
            this.Evaluation.Click += new System.EventHandler(this.Evaluation_Click);
            // 
            // assessmentPanel
            // 
            this.assessmentPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.assessmentPanel.Controls.Add(this.assessmentComponentBtn);
            this.assessmentPanel.Controls.Add(this.assessmentListBtn);
            this.assessmentPanel.Controls.Add(this.addAssessmentComponent);
            this.assessmentPanel.Controls.Add(this.addAssessment);
            this.assessmentPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.assessmentPanel.Location = new System.Drawing.Point(0, 523);
            this.assessmentPanel.Name = "assessmentPanel";
            this.assessmentPanel.Size = new System.Drawing.Size(183, 128);
            this.assessmentPanel.TabIndex = 8;
            // 
            // assessmentComponentBtn
            // 
            this.assessmentComponentBtn.BackColor = System.Drawing.Color.Gray;
            this.assessmentComponentBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.assessmentComponentBtn.FlatAppearance.BorderSize = 0;
            this.assessmentComponentBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.assessmentComponentBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.assessmentComponentBtn.Location = new System.Drawing.Point(0, 99);
            this.assessmentComponentBtn.Name = "assessmentComponentBtn";
            this.assessmentComponentBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.assessmentComponentBtn.Size = new System.Drawing.Size(183, 33);
            this.assessmentComponentBtn.TabIndex = 8;
            this.assessmentComponentBtn.Text = "Component List";
            this.assessmentComponentBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.assessmentComponentBtn.UseVisualStyleBackColor = false;
            this.assessmentComponentBtn.Click += new System.EventHandler(this.assessmentComponentBtn_Click);
            // 
            // assessmentListBtn
            // 
            this.assessmentListBtn.BackColor = System.Drawing.Color.Gray;
            this.assessmentListBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.assessmentListBtn.FlatAppearance.BorderSize = 0;
            this.assessmentListBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.assessmentListBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.assessmentListBtn.Location = new System.Drawing.Point(0, 66);
            this.assessmentListBtn.Name = "assessmentListBtn";
            this.assessmentListBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.assessmentListBtn.Size = new System.Drawing.Size(183, 33);
            this.assessmentListBtn.TabIndex = 7;
            this.assessmentListBtn.Text = "Assessment List";
            this.assessmentListBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.assessmentListBtn.UseVisualStyleBackColor = false;
            this.assessmentListBtn.Click += new System.EventHandler(this.assessmentListBtn_Click);
            // 
            // addAssessmentComponent
            // 
            this.addAssessmentComponent.BackColor = System.Drawing.Color.Gray;
            this.addAssessmentComponent.Dock = System.Windows.Forms.DockStyle.Top;
            this.addAssessmentComponent.FlatAppearance.BorderSize = 0;
            this.addAssessmentComponent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addAssessmentComponent.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addAssessmentComponent.Location = new System.Drawing.Point(0, 33);
            this.addAssessmentComponent.Name = "addAssessmentComponent";
            this.addAssessmentComponent.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.addAssessmentComponent.Size = new System.Drawing.Size(183, 33);
            this.addAssessmentComponent.TabIndex = 5;
            this.addAssessmentComponent.Text = "Add Component";
            this.addAssessmentComponent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addAssessmentComponent.UseVisualStyleBackColor = false;
            this.addAssessmentComponent.Click += new System.EventHandler(this.assessmentList_Click_1);
            // 
            // addAssessment
            // 
            this.addAssessment.BackColor = System.Drawing.Color.Gray;
            this.addAssessment.Dock = System.Windows.Forms.DockStyle.Top;
            this.addAssessment.FlatAppearance.BorderSize = 0;
            this.addAssessment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addAssessment.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addAssessment.Location = new System.Drawing.Point(0, 0);
            this.addAssessment.Name = "addAssessment";
            this.addAssessment.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.addAssessment.Size = new System.Drawing.Size(183, 33);
            this.addAssessment.TabIndex = 1;
            this.addAssessment.Text = "Add Assessment";
            this.addAssessment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addAssessment.UseVisualStyleBackColor = false;
            this.addAssessment.Click += new System.EventHandler(this.button6_Click);
            // 
            // Assessments
            // 
            this.Assessments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.Assessments.Dock = System.Windows.Forms.DockStyle.Top;
            this.Assessments.FlatAppearance.BorderSize = 0;
            this.Assessments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Assessments.ForeColor = System.Drawing.SystemColors.Control;
            this.Assessments.Location = new System.Drawing.Point(0, 493);
            this.Assessments.Name = "Assessments";
            this.Assessments.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.Assessments.Size = new System.Drawing.Size(183, 30);
            this.Assessments.TabIndex = 7;
            this.Assessments.Text = "Manage Assessments";
            this.Assessments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Assessments.UseVisualStyleBackColor = false;
            this.Assessments.Click += new System.EventHandler(this.button7_Click);
            // 
            // rubricsPanel
            // 
            this.rubricsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.rubricsPanel.Controls.Add(this.rubricLevelListBtn);
            this.rubricsPanel.Controls.Add(this.addRubricLevel);
            this.rubricsPanel.Controls.Add(this.rubricListBtn);
            this.rubricsPanel.Controls.Add(this.addRubricbtn);
            this.rubricsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.rubricsPanel.Location = new System.Drawing.Point(0, 358);
            this.rubricsPanel.Name = "rubricsPanel";
            this.rubricsPanel.Size = new System.Drawing.Size(183, 135);
            this.rubricsPanel.TabIndex = 6;
            // 
            // rubricLevelListBtn
            // 
            this.rubricLevelListBtn.BackColor = System.Drawing.Color.Gray;
            this.rubricLevelListBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.rubricLevelListBtn.FlatAppearance.BorderSize = 0;
            this.rubricLevelListBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rubricLevelListBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rubricLevelListBtn.Location = new System.Drawing.Point(0, 99);
            this.rubricLevelListBtn.Name = "rubricLevelListBtn";
            this.rubricLevelListBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.rubricLevelListBtn.Size = new System.Drawing.Size(183, 33);
            this.rubricLevelListBtn.TabIndex = 5;
            this.rubricLevelListBtn.Text = "Rubrics Level List";
            this.rubricLevelListBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rubricLevelListBtn.UseVisualStyleBackColor = false;
            this.rubricLevelListBtn.Click += new System.EventHandler(this.rubricLevelListBtn_Click);
            // 
            // addRubricLevel
            // 
            this.addRubricLevel.BackColor = System.Drawing.Color.Gray;
            this.addRubricLevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.addRubricLevel.FlatAppearance.BorderSize = 0;
            this.addRubricLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addRubricLevel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addRubricLevel.Location = new System.Drawing.Point(0, 66);
            this.addRubricLevel.Name = "addRubricLevel";
            this.addRubricLevel.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.addRubricLevel.Size = new System.Drawing.Size(183, 33);
            this.addRubricLevel.TabIndex = 4;
            this.addRubricLevel.Text = "Add Rubric Level";
            this.addRubricLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addRubricLevel.UseVisualStyleBackColor = false;
            this.addRubricLevel.Click += new System.EventHandler(this.addRubricLevel_Click_1);
            // 
            // rubricListBtn
            // 
            this.rubricListBtn.BackColor = System.Drawing.Color.Gray;
            this.rubricListBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.rubricListBtn.FlatAppearance.BorderSize = 0;
            this.rubricListBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rubricListBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rubricListBtn.Location = new System.Drawing.Point(0, 33);
            this.rubricListBtn.Name = "rubricListBtn";
            this.rubricListBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.rubricListBtn.Size = new System.Drawing.Size(183, 33);
            this.rubricListBtn.TabIndex = 2;
            this.rubricListBtn.Text = "Rubrics List";
            this.rubricListBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rubricListBtn.UseVisualStyleBackColor = false;
            this.rubricListBtn.Click += new System.EventHandler(this.rubricListBtn_Click);
            // 
            // addRubricbtn
            // 
            this.addRubricbtn.BackColor = System.Drawing.Color.Gray;
            this.addRubricbtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.addRubricbtn.FlatAppearance.BorderSize = 0;
            this.addRubricbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addRubricbtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addRubricbtn.Location = new System.Drawing.Point(0, 0);
            this.addRubricbtn.Name = "addRubricbtn";
            this.addRubricbtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.addRubricbtn.Size = new System.Drawing.Size(183, 33);
            this.addRubricbtn.TabIndex = 1;
            this.addRubricbtn.Text = "Add Rubrics";
            this.addRubricbtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addRubricbtn.UseVisualStyleBackColor = false;
            this.addRubricbtn.Click += new System.EventHandler(this.addRubricbtn_Click);
            // 
            // rubrics
            // 
            this.rubrics.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.rubrics.Dock = System.Windows.Forms.DockStyle.Top;
            this.rubrics.FlatAppearance.BorderSize = 0;
            this.rubrics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rubrics.ForeColor = System.Drawing.SystemColors.Control;
            this.rubrics.Location = new System.Drawing.Point(0, 328);
            this.rubrics.Name = "rubrics";
            this.rubrics.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.rubrics.Size = new System.Drawing.Size(183, 30);
            this.rubrics.TabIndex = 5;
            this.rubrics.Text = "Manage Rubrics";
            this.rubrics.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rubrics.UseVisualStyleBackColor = false;
            this.rubrics.Click += new System.EventHandler(this.rubrics_Click_1);
            // 
            // courseSubPanel
            // 
            this.courseSubPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.courseSubPanel.Controls.Add(this.cloListBtn);
            this.courseSubPanel.Controls.Add(this.addCloBtn);
            this.courseSubPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.courseSubPanel.Location = new System.Drawing.Point(0, 261);
            this.courseSubPanel.Name = "courseSubPanel";
            this.courseSubPanel.Size = new System.Drawing.Size(183, 67);
            this.courseSubPanel.TabIndex = 4;
            // 
            // cloListBtn
            // 
            this.cloListBtn.BackColor = System.Drawing.Color.Gray;
            this.cloListBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.cloListBtn.FlatAppearance.BorderSize = 0;
            this.cloListBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cloListBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cloListBtn.Location = new System.Drawing.Point(0, 33);
            this.cloListBtn.Name = "cloListBtn";
            this.cloListBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.cloListBtn.Size = new System.Drawing.Size(183, 33);
            this.cloListBtn.TabIndex = 3;
            this.cloListBtn.Text = "Clos List";
            this.cloListBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cloListBtn.UseVisualStyleBackColor = false;
            this.cloListBtn.Click += new System.EventHandler(this.cloListBtn_Click);
            // 
            // addCloBtn
            // 
            this.addCloBtn.BackColor = System.Drawing.Color.Gray;
            this.addCloBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.addCloBtn.FlatAppearance.BorderSize = 0;
            this.addCloBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addCloBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addCloBtn.Location = new System.Drawing.Point(0, 0);
            this.addCloBtn.Name = "addCloBtn";
            this.addCloBtn.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.addCloBtn.Size = new System.Drawing.Size(183, 33);
            this.addCloBtn.TabIndex = 1;
            this.addCloBtn.Text = "Add CLOs";
            this.addCloBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addCloBtn.UseVisualStyleBackColor = false;
            this.addCloBtn.Click += new System.EventHandler(this.crsbutton1_Click);
            // 
            // CLOs
            // 
            this.CLOs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.CLOs.Dock = System.Windows.Forms.DockStyle.Top;
            this.CLOs.FlatAppearance.BorderSize = 0;
            this.CLOs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CLOs.ForeColor = System.Drawing.SystemColors.Control;
            this.CLOs.Location = new System.Drawing.Point(0, 231);
            this.CLOs.Name = "CLOs";
            this.CLOs.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.CLOs.Size = new System.Drawing.Size(183, 30);
            this.CLOs.TabIndex = 2;
            this.CLOs.Text = "Manage CLOs";
            this.CLOs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CLOs.UseVisualStyleBackColor = false;
            this.CLOs.Click += new System.EventHandler(this.coursesBtn_Click);
            // 
            // subPanelStudent
            // 
            this.subPanelStudent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.subPanelStudent.Controls.Add(this.stdbutton2);
            this.subPanelStudent.Controls.Add(this.stdbutton1);
            this.subPanelStudent.Dock = System.Windows.Forms.DockStyle.Top;
            this.subPanelStudent.Location = new System.Drawing.Point(0, 165);
            this.subPanelStudent.Name = "subPanelStudent";
            this.subPanelStudent.Size = new System.Drawing.Size(183, 66);
            this.subPanelStudent.TabIndex = 1;
            // 
            // stdbutton2
            // 
            this.stdbutton2.BackColor = System.Drawing.Color.Gray;
            this.stdbutton2.Dock = System.Windows.Forms.DockStyle.Top;
            this.stdbutton2.FlatAppearance.BorderSize = 0;
            this.stdbutton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stdbutton2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.stdbutton2.Location = new System.Drawing.Point(0, 33);
            this.stdbutton2.Name = "stdbutton2";
            this.stdbutton2.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.stdbutton2.Size = new System.Drawing.Size(183, 31);
            this.stdbutton2.TabIndex = 3;
            this.stdbutton2.Text = "Student List";
            this.stdbutton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.stdbutton2.UseVisualStyleBackColor = false;
            this.stdbutton2.Click += new System.EventHandler(this.stdbutton2_Click_1);
            // 
            // stdbutton1
            // 
            this.stdbutton1.BackColor = System.Drawing.Color.Gray;
            this.stdbutton1.Dock = System.Windows.Forms.DockStyle.Top;
            this.stdbutton1.FlatAppearance.BorderSize = 0;
            this.stdbutton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stdbutton1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.stdbutton1.Location = new System.Drawing.Point(0, 0);
            this.stdbutton1.Name = "stdbutton1";
            this.stdbutton1.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.stdbutton1.Size = new System.Drawing.Size(183, 33);
            this.stdbutton1.TabIndex = 1;
            this.stdbutton1.Text = "Add Student";
            this.stdbutton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.stdbutton1.UseVisualStyleBackColor = false;
            this.stdbutton1.Click += new System.EventHandler(this.stdbutton1_Click);
            // 
            // upperLeftPanel
            // 
            this.upperLeftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.upperLeftPanel.Controls.Add(this.pictureBox1);
            this.upperLeftPanel.Controls.Add(this.panel3);
            this.upperLeftPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.upperLeftPanel.Location = new System.Drawing.Point(0, 0);
            this.upperLeftPanel.Name = "upperLeftPanel";
            this.upperLeftPanel.Size = new System.Drawing.Size(183, 135);
            this.upperLeftPanel.TabIndex = 1;
            this.upperLeftPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.upperLeftPanel_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(35, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 99);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(3, 135);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 43);
            this.panel3.TabIndex = 2;
            // 
            // upperCoverPanel
            // 
            this.upperCoverPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.upperCoverPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.upperCoverPanel.Location = new System.Drawing.Point(200, 0);
            this.upperCoverPanel.Name = "upperCoverPanel";
            this.upperCoverPanel.Size = new System.Drawing.Size(684, 30);
            this.upperCoverPanel.TabIndex = 1;
            this.upperCoverPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.upperCoverPanel_Paint);
            // 
            // belowPanel
            // 
            this.belowPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(26)))), ((int)(((byte)(51)))));
            this.belowPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.belowPanel.Location = new System.Drawing.Point(200, 505);
            this.belowPanel.Name = "belowPanel";
            this.belowPanel.Size = new System.Drawing.Size(684, 36);
            this.belowPanel.TabIndex = 2;
            this.belowPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.belowPanel_Paint_1);
            // 
            // childFormPanel
            // 
            this.childFormPanel.BackColor = System.Drawing.Color.White;
            this.childFormPanel.Controls.Add(this.tableLayoutPanel1);
            this.childFormPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.childFormPanel.Location = new System.Drawing.Point(200, 30);
            this.childFormPanel.Name = "childFormPanel";
            this.childFormPanel.Size = new System.Drawing.Size(684, 475);
            this.childFormPanel.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 451F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(684, 475);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(117, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(564, 451);
            this.label1.TabIndex = 2;
            this.label1.Text = "Student And\r\nResult Management\r\nSystem";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(884, 541);
            this.Controls.Add(this.childFormPanel);
            this.Controls.Add(this.belowPanel);
            this.Controls.Add(this.upperCoverPanel);
            this.Controls.Add(this.leftSidePanel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.leftSidePanel.ResumeLayout(false);
            this.manageAttendancepanel.ResumeLayout(false);
            this.evaluationPanel.ResumeLayout(false);
            this.assessmentPanel.ResumeLayout(false);
            this.rubricsPanel.ResumeLayout(false);
            this.courseSubPanel.ResumeLayout(false);
            this.subPanelStudent.ResumeLayout(false);
            this.upperLeftPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.childFormPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel leftSidePanel;
        private System.Windows.Forms.Panel upperLeftPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel subPanelStudent;
        private System.Windows.Forms.Button stdbutton1;
        private System.Windows.Forms.Button CLOs;
        private System.Windows.Forms.Button addCloBtn;
        private System.Windows.Forms.Button cloListBtn;
        private System.Windows.Forms.Panel courseSubPanel;
        private System.Windows.Forms.Panel upperCoverPanel;
        private System.Windows.Forms.Panel belowPanel;
        private System.Windows.Forms.Panel childFormPanel;
        private System.Windows.Forms.Panel rubricsPanel;
        private System.Windows.Forms.Button rubricListBtn;
        private System.Windows.Forms.Button addRubricbtn;
        private System.Windows.Forms.Button rubrics;
        private System.Windows.Forms.Panel assessmentPanel;
        private System.Windows.Forms.Button addAssessment;
        private System.Windows.Forms.Button Assessments;
        private System.Windows.Forms.Panel evaluationPanel;
        private System.Windows.Forms.Button Evaluation;
        private System.Windows.Forms.Button addAssessmentComponent;
        private System.Windows.Forms.Button stdbutton2;
        private System.Windows.Forms.Panel manageAttendancepanel;
        private System.Windows.Forms.Button attendanceListBtn;
        private System.Windows.Forms.Button addStudentAttendanceBtn;
        private System.Windows.Forms.Button addDateBtn;
        private System.Windows.Forms.Button StudentAttendance;
        private System.Windows.Forms.Button addRubricLevel;
        private System.Windows.Forms.Button rubricLevelListBtn;
        private System.Windows.Forms.Button assessmentComponentBtn;
        private System.Windows.Forms.Button assessmentListBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button evaluateStudentListBtn;
        private System.Windows.Forms.Button evaluateStudentBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

