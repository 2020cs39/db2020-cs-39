﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text.RegularExpressions;
using System.IO;

namespace DB2020_CS_39
{
    public partial class StudentListForm : Form
    {
        public StudentListForm()
        {
            InitializeComponent();
        }
        private Form1 f1 = new Form1();
        private string queryForStudentTable = "Select S.FirstName, S.LastName, S.Contact, S.Email, S.RegistrationNumber As [Registration Number], S.Status As [Status], SUM((CAST(SelectedLevel.MeasurementLevel As Decimal) / MaxLevel.MaxRubricLevel) * TotalMarks.TotalMarks) As[Total Marks] From Student As S Left Join StudentResult As USr On Id = StudentId Left Join(Select MeasurementLevel, Rl.Id As RMId , StudentId, Rl.RubricId From StudentResult As Sr Join RubricLevel As Rl On Rl.Id = Sr.RubricMeasurementId) As SelectedLevel On SelectedLevel.RMId = USr.RubricMeasurementId Left Join(Select MAX(MeasurementLevel) As MaxRubricLevel, Rl.RubricId From StudentResult As Sr, AssessmentComponent As Ac, RubricLevel As Rl Where Sr.AssessmentComponentId = Ac.Id	And Rl.RubricId = Ac.RubricId Group By Rl.RubricId) As MaxLevel On MaxLevel.RubricId = SelectedLevel.RubricId Left Join(Select TotalMarks, Ac.Id As AcID From StudentResult As Sr, AssessmentComponent As Ac   Where Sr.AssessmentComponentId = Ac.Id) As TotalMarks On USr.AssessmentComponentId = TotalMarks.AcID Group By S.FirstName, S.LastName, S.Contact, S.Email, S.RegistrationNumber, S.Status";

        private bool isValidName(string n)
        {
            bool isValid = false;
            Regex checkName = new Regex(@"^([A-Z][a-z-A-Z]+)$");
            isValid = checkName.IsMatch(n);
            return isValid;
        }
        private bool isValidMail(string e)
        {
            bool isValidMail = false;
            Regex checkEmail = new Regex(@"^([w.-]+)@ ([w-]+) ((. (w) {2,3})+)$");
            isValidMail = checkEmail.IsMatch(e);
            return isValidMail;
        }

        private void StudentListForm_Load(object sender, EventArgs e)
        {
            f1.loaDdataIntoGrid(stdDataGridView,queryForStudentTable);
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string forConversion = "";
                string selected_reg = stdDataGridView.CurrentRow.Cells[4].Value.ToString();
                int selected_Id = 0;
                string findStdQuery = "Select Id From Student Where RegistrationNumber Like '" + selected_reg + "' ";
                var conRubric = Configuration.getInstance().getConnection();
                SqlCommand findRubricID = new SqlCommand(findStdQuery, conRubric);
                SqlDataReader RIDreader = findRubricID.ExecuteReader();
                if (RIDreader.HasRows)
                {
                    while (RIDreader.Read())
                    {
                        forConversion = RIDreader["Id"].ToString();
                        selected_Id = int.Parse(forConversion);
                    }
                }
                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand(("DELETE FROM StudentResult WHERE StudentId = '" + selected_Id + "' ")+("DELETE FROM StudentAttendance WHERE StudentId = '" + selected_Id + "' ")+("DELETE FROM Student WHERE Id = '" + selected_Id + "' "), con1);
                cmd1.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(stdDataGridView, queryForStudentTable); // This function call will refresh grid
            }
            
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int upStatus = 0;
                string forConversion = "";
                string selected_fName = stdDataGridView.CurrentRow.Cells[0].Value.ToString();
                string selected_lName = stdDataGridView.CurrentRow.Cells[1].Value.ToString();
                string selected_con = stdDataGridView.CurrentRow.Cells[2].Value.ToString();
                string selected_email = stdDataGridView.CurrentRow.Cells[3].Value.ToString();
                string selected_reg = stdDataGridView.CurrentRow.Cells[4].Value.ToString();
                string selected_status = stdDataGridView.CurrentRow.Cells[5].Value.ToString();
                int selected_Id = 0;
                string findStdQuery = "Select Id From Student Where RegistrationNumber Like '" + selected_reg + "' ";
                var conRubric = Configuration.getInstance().getConnection();
                SqlCommand findRubricID = new SqlCommand(findStdQuery, conRubric);
                SqlDataReader RIDreader = findRubricID.ExecuteReader();
                if (RIDreader.HasRows)
                {
                    while (RIDreader.Read())
                    {
                        forConversion = RIDreader["Id"].ToString();
                        selected_Id = int.Parse(forConversion);
                    }
                }
                RIDreader.Close();
                bool allAttributeValid = true;


                if (!isValidName(selected_fName))
                {
                    MessageBox.Show("First Name is not in correct Format");
                    allAttributeValid = false;
                }
                if (!isValidName(selected_lName))
                {
                    MessageBox.Show("Last Name is not in correct Format");
                    allAttributeValid = false;
                }
                if (selected_status != "5" && selected_status != "6")
                {
                    MessageBox.Show("Status should be 5 or 6");
                    allAttributeValid = false;
                }
                if (selected_status == "5")
                {
                    upStatus = 5;
                }
                if (selected_status == "6")
                {
                    upStatus = 6;
                }
                if (allAttributeValid)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("UPDATE Student SET FirstName =  @FirstName, LastName = @LastName , Contact = @Contact, Email = @Email, RegistrationNumber = @RegistrationNumber, Status = @Status WHERE Id = '" + selected_Id + "' ", con);
                    cmd.Parameters.AddWithValue("@FirstName", selected_fName);
                    cmd.Parameters.AddWithValue("@LastName", selected_lName);
                    cmd.Parameters.AddWithValue("@Contact", selected_con);
                    cmd.Parameters.AddWithValue("@Email", selected_email);
                    cmd.Parameters.AddWithValue("@RegistrationNumber", selected_reg);
                    cmd.Parameters.AddWithValue("@Status", upStatus);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Updated");
                }
                
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(stdDataGridView, queryForStudentTable); // This function call will refresh grid
            }

        }

        private void exportPdfBtn_Click(object sender, EventArgs e)
        {
            if (stdDataGridView.Rows.Count > 0)
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.Filter = "PDF (*.pdf)|*.pdf";
                saveFile.FileName = "StudentResult.pdf";
                bool isNotValid = false;

                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(saveFile.FileName))
                    {
                        try
                        {
                            File.Delete(saveFile.FileName);
                        }
                        catch (Exception exp)
                        {
                            isNotValid = true;
                            MessageBox.Show("Unable write data in pdf" + exp.Message);
                        }
                    }
                    if (!isNotValid)
                    {
                        try
                        {
                            PdfPTable pTable = new PdfPTable(stdDataGridView.Columns.Count);
                            pTable.DefaultCell.Padding = 4;
                            pTable.WidthPercentage = 100;
                            pTable.HorizontalAlignment = Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn column in stdDataGridView.Columns)
                            {
                                PdfPCell pCell = new PdfPCell(new Phrase(column.HeaderText));
                                pTable.AddCell(pCell);
                            }

                            foreach (DataGridViewRow viewRow in stdDataGridView.Rows)
                            {
                                foreach (DataGridViewCell dcell in viewRow.Cells)
                                {
                                    pTable.AddCell(dcell.Value.ToString());
                                }
                            }
                            Paragraph space = new Paragraph("     ");
                            iTextSharp.text.Font headerFont = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            Paragraph p = new Paragraph("All Student Result", headerFont);
                            p.Alignment = Element.ALIGN_CENTER;
                            using (FileStream fileStream = new FileStream(saveFile.FileName, FileMode.Create))
                            {
                                Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);
                                PdfWriter.GetInstance(document, fileStream);
                                document.Open();
                                document.Add(pTable);
                                document.Close();
                                fileStream.Close();
                            }
                            MessageBox.Show("Pdf Successfully saved");
                        }
                        catch (Exception exp)

                        {
                            MessageBox.Show("Error while exporting Data" + exp.Message);
                        }

                    }

                }

            }

            else

            {

                MessageBox.Show("No Record Found", "Info");

            }
        }
    }
}
