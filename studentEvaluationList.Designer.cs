﻿
namespace DB2020_CS_39
{
    partial class studentEvaluationList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.studentEvaluationGrid = new System.Windows.Forms.DataGridView();
            this.exportREsultPdf = new System.Windows.Forms.Button();
            this.stdListHeadPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentEvaluationGrid)).BeginInit();
            this.stdListHeadPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.studentEvaluationGrid, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.exportREsultPdf, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(90, 139);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(510, 324);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // studentEvaluationGrid
            // 
            this.studentEvaluationGrid.AllowUserToAddRows = false;
            this.studentEvaluationGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.studentEvaluationGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.studentEvaluationGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel1.SetColumnSpan(this.studentEvaluationGrid, 3);
            this.studentEvaluationGrid.Location = new System.Drawing.Point(3, 67);
            this.studentEvaluationGrid.Name = "studentEvaluationGrid";
            this.studentEvaluationGrid.RowTemplate.Height = 25;
            this.studentEvaluationGrid.Size = new System.Drawing.Size(504, 254);
            this.studentEvaluationGrid.TabIndex = 20;
            // 
            // exportREsultPdf
            // 
            this.exportREsultPdf.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.exportREsultPdf.BackColor = System.Drawing.Color.DodgerBlue;
            this.exportREsultPdf.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.exportREsultPdf.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.exportREsultPdf.Location = new System.Drawing.Point(173, 13);
            this.exportREsultPdf.Name = "exportREsultPdf";
            this.exportREsultPdf.Size = new System.Drawing.Size(120, 38);
            this.exportREsultPdf.TabIndex = 21;
            this.exportREsultPdf.Text = "Export PDF";
            this.exportREsultPdf.UseVisualStyleBackColor = false;
            this.exportREsultPdf.Click += new System.EventHandler(this.exportREsultPdf_Click);
            // 
            // stdListHeadPanel
            // 
            this.stdListHeadPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stdListHeadPanel.ColumnCount = 3;
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.25316F));
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.49368F));
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.25316F));
            this.stdListHeadPanel.Controls.Add(this.label1, 1, 0);
            this.stdListHeadPanel.Location = new System.Drawing.Point(12, 12);
            this.stdListHeadPanel.Name = "stdListHeadPanel";
            this.stdListHeadPanel.RowCount = 1;
            this.stdListHeadPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.stdListHeadPanel.Size = new System.Drawing.Size(656, 87);
            this.stdListHeadPanel.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(168, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(318, 87);
            this.label1.TabIndex = 0;
            this.label1.Text = "Student Evaluation List";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // studentEvaluationList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 475);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.stdListHeadPanel);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "studentEvaluationList";
            this.Text = "studentEvaluationList";
            this.Load += new System.EventHandler(this.studentEvaluationList_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.studentEvaluationGrid)).EndInit();
            this.stdListHeadPanel.ResumeLayout(false);
            this.stdListHeadPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel stdListHeadPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView studentEvaluationGrid;
        private System.Windows.Forms.Button exportREsultPdf;
    }
}