﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class studentEvaluationList : Form
    {
        public studentEvaluationList()
        {
            InitializeComponent();
        }
        private Form1 f1 = new Form1();
        private string querytoLoadResult = "Select S.RegistrationNumber As[Registration Number], TotalMarks.acname As [Assessment Component], SUM((CAST(SelectedLevel.MeasurementLevel As Decimal)/MaxLevel.MaxRubricLevel)*TotalMarks.TotalMarks) As[Total Marks] From Student As S Left Join StudentResult As USr On Id = StudentId Left Join(Select MeasurementLevel, Rl.Id As RMId , StudentId, Rl.RubricId From StudentResult As Sr Join RubricLevel As Rl On Rl.Id = Sr.RubricMeasurementId) As SelectedLevel On SelectedLevel.RMId = USr.RubricMeasurementId Left Join (Select MAX(MeasurementLevel) As MaxRubricLevel, Rl.RubricId From StudentResult As Sr, AssessmentComponent As Ac, RubricLevel As Rl Where Sr.AssessmentComponentId = Ac.Id And Rl.RubricId = Ac.RubricId Group By Rl.RubricId) As MaxLevel On MaxLevel.RubricId = SelectedLevel.RubricId Left Join (Select TotalMarks, Ac.Id As AcID, ac.Name as acname From StudentResult As Sr, AssessmentComponent As Ac Where Sr.AssessmentComponentId = Ac.Id) As TotalMarks On USr.AssessmentComponentId = TotalMarks.AcID Group By S.RegistrationNumber , TotalMarks.acname Having SUM((CAST(SelectedLevel.MeasurementLevel As Decimal)/MaxLevel.MaxRubricLevel)*TotalMarks.TotalMarks) is not null;";
        private void studentEvaluationList_Load(object sender, EventArgs e)
        {

            f1.loaDdataIntoGrid(studentEvaluationGrid, querytoLoadResult);

        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
           
            
        }

        private void exportREsultPdf_Click(object sender, EventArgs e)
        {
            if (studentEvaluationGrid.Rows.Count > 0)
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.Filter = "PDF (*.pdf)|*.pdf";
                saveFile.FileName = "Result.pdf";
                bool isNotValid = false;

                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(saveFile.FileName))
                    {
                        try
                        {
                            File.Delete(saveFile.FileName);
                        }
                        catch (Exception exp)
                        {
                            isNotValid = true;
                            MessageBox.Show("Unable write in pdf" + exp.Message);
                        }
                    }
                    if (!isNotValid)
                    {
                        try
                        {
                            PdfPTable pTable = new PdfPTable(studentEvaluationGrid.Columns.Count);
                            pTable.DefaultCell.Padding = 8;
                            pTable.WidthPercentage = 100;
                            pTable.HorizontalAlignment = Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn column in studentEvaluationGrid.Columns)
                            {
                                PdfPCell pCell = new PdfPCell(new Phrase(column.HeaderText));
                                pTable.AddCell(pCell);
                            }

                            foreach (DataGridViewRow viewRow in studentEvaluationGrid.Rows)
                            {
                                foreach (DataGridViewCell dcell in viewRow.Cells)
                                {
                                    pTable.AddCell(dcell.Value.ToString());
                                }
                            }
                            Paragraph space = new Paragraph("     ");
                            iTextSharp.text.Font headerFont = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            Paragraph p = new Paragraph("Result of students Which was evaluated", headerFont);
                            p.Alignment = Element.ALIGN_CENTER;
                            using (FileStream fileStream = new FileStream(saveFile.FileName, FileMode.Create))
                            {
                                Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);
                                PdfWriter.GetInstance(document, fileStream);
                                document.Open();
                                document.Add(space);
                                document.Add(p);
                                document.Add(space);
                                document.Add(pTable);
                                document.Close();
                                fileStream.Close();
                            }
                            MessageBox.Show("Pdf Successfully saved");
                        }
                        catch (Exception exp)

                        {
                            MessageBox.Show("Error while exporting Data" + exp.Message);
                        }

                    }

                }

            }

            else

            {

                MessageBox.Show("No Record Found", "Info");

            }
        }
    }
}
