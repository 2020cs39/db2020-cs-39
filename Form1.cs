﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            onLoadFormWillBe();
        }

        public void loaDdataIntoGrid(DataGridView grid , string query)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            grid.DataSource = dt;
        }

        private void onLoadFormWillBe()
        {
            subPanelStudent.Visible = false;
            courseSubPanel.Visible = false;
            rubricsPanel.Visible = false;
            assessmentPanel.Visible = false;
            evaluationPanel.Visible = false;
            manageAttendancepanel.Visible = false;
       
        }

        private void hideSubMenu()
        {
            if(subPanelStudent.Visible == true)
            {
                subPanelStudent.Visible = false;
            }
            if(courseSubPanel.Visible == true)
            {
                courseSubPanel.Visible = false;
            }
            if(rubricsPanel.Visible == true)
            {
                rubricsPanel.Visible = false;
            }
            if (assessmentPanel.Visible == true)
            {
                assessmentPanel.Visible = false;
            }
            if (evaluationPanel.Visible == true)
            {
                evaluationPanel.Visible = false;
            }
            if(manageAttendancepanel.Visible == true)
            {
                manageAttendancepanel.Visible = false;
            }

        }

        private void showSubMenu(Panel p)
        {
            if(p.Visible == false)
            {
                hideSubMenu();
                p.Visible = true;
            }
            else
            {
                p.Visible = false;
            }
        }



        private void studentSubPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Student_Click(object sender, EventArgs e)
        {
            showSubMenu(subPanelStudent);
        }

        private void stdbutton1_Click(object sender, EventArgs e)
        {
            openFormInPanel(new addStudentFrom());
            // code for operations
            hideSubMenu();
        }

        private void stdbutton2_Click(object sender, EventArgs e)
        {
            openFormInPanel(new StudentListForm());
            hideSubMenu();
        }

        private void stdbutton3_Click(object sender, EventArgs e)
        {
            // code for operations
            hideSubMenu();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            // code for operations
            hideSubMenu();
        }

        private void coursesBtn_Click(object sender, EventArgs e)
        {
            showSubMenu(courseSubPanel);
        }

        private void crsbutton1_Click(object sender, EventArgs e)
        {
            // code for operations
            openFormInPanel(new addClos());
            hideSubMenu();
        }

        private Form recentForm = null;
        private void openFormInPanel(Form childForm)
        {
            if (recentForm != null)
            {
                recentForm.Close();
            }
            recentForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            childFormPanel.Controls.Add(childForm);
            childFormPanel.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void childFormPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void leftSidePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Rubrics_Click(object sender, EventArgs e)
        {

        }

        private void courseSubPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void subPanelStudent_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void upperCover_Paint(object sender, PaintEventArgs e)
        {

        }

        private void belowPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rubrics_Click_1(object sender, EventArgs e)
        {
            showSubMenu(rubricsPanel);
        }

        private void addRubricbtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new addRubrics());
            hideSubMenu();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            showSubMenu(assessmentPanel);

        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            openFormInPanel(new addAssessment());
            hideSubMenu();
        }

        private void assessmentList_Click(object sender, EventArgs e)
        {

        }

        private void assessmentList_Click_1(object sender, EventArgs e)
        {
            openFormInPanel(new AddAssessmentComponent());
            hideSubMenu();
        }

        private void AssessmentList_Click_2(object sender, EventArgs e)
        {

        }

        private void addRubricLevel_Click(object sender, EventArgs e)
        {
            openFormInPanel(new AddRubricLevel());
            hideSubMenu();
        }

        private void assessmentList_Click_3(object sender, EventArgs e)
        {
            openFormInPanel(new assessmentList());
            hideSubMenu();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void upperLeftPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Evaluation_Click(object sender, EventArgs e)
        {
            showSubMenu(evaluationPanel);
        }

        private void upperCoverPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void belowPanel_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void evaluateStudentBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new evaluateStudent());
            hideSubMenu();
        }

        private void addAttendancedate_Click(object sender, EventArgs e)
        {
            openFormInPanel(new addClassAttendance());
            hideSubMenu();
        }

        private void stdbutton2_Click_1(object sender, EventArgs e)
        {
            openFormInPanel(new StudentListForm());
            hideSubMenu();
        }

        private void rubricListBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new rubricList());
            hideSubMenu();
        }

        private void cloListBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new CloList());
            hideSubMenu();
        }

        private void StudentAttendance_Click(object sender, EventArgs e)
        {
            showSubMenu(manageAttendancepanel);
        }

        private void addDateBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new addClassAttendance());
            hideSubMenu();
        }

        private void addStudentAttendanceBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new addStudentAttendance());
            hideSubMenu();
        }

        private void addRubricLevel_Click_1(object sender, EventArgs e)
        {
            openFormInPanel(new AddRubricLevel());
            hideSubMenu();
        }

        private void rubricLevelListBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new rubricLevelList());
            hideSubMenu();
        }

        private void attendanceListBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new attendanceList());
            hideSubMenu();
        }

        private void assessmentListBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new assessmentList());
            hideSubMenu();
        }

        private void assessmentComponentBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new assessmentComponentList());
            hideSubMenu();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void evaluateStudentBtn_Click_1(object sender, EventArgs e)
        {
            openFormInPanel(new evaluateStudent());
            hideSubMenu();
        }

        private void evaluateStudentListBtn_Click(object sender, EventArgs e)
        {
            openFormInPanel(new studentEvaluationList());
            hideSubMenu();
        }
    }
}
