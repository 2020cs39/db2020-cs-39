﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class evaluateStudent : Form
    {
        public evaluateStudent()
        {
            InitializeComponent();
        }
        /* Dictionaries for storing students assessment component and rubric measurement*/
        private Dictionary<string, string> std = new Dictionary<string, string>();
        private Dictionary<string, string> ac = new Dictionary<string, string>();
        private Dictionary<string, string> rm = new Dictionary<string, string>();
        private void evaluateStudent_Load(object sender, EventArgs e) /*On Add Button click event*/
        {
            var conA = Configuration.getInstance().getConnection();
            SqlCommand findAssessmenCtID = new SqlCommand("Select Id,Name From AssessmentComponent", conA);
            SqlDataReader ACIDreader = findAssessmenCtID.ExecuteReader();
            if (ACIDreader.HasRows)
            {
                while (ACIDreader.Read())
                {
                    ac.Add(ACIDreader["Id"].ToString(), ACIDreader["Name"].ToString());
                    assessmentComboBox.Items.Add(ACIDreader["Name"].ToString());
                }
            }
            ACIDreader.Close();


            var conStdA = Configuration.getInstance().getConnection();
            SqlCommand findAStudentID = new SqlCommand("Select Id,RegistrationNumber From Student Where Status = 5", conStdA);
            SqlDataReader StdAIDreader = findAStudentID.ExecuteReader();
            if (StdAIDreader.HasRows)
            {
                while (StdAIDreader.Read())
                {
                    std.Add(StdAIDreader["Id"].ToString(), StdAIDreader["RegistrationNumber"].ToString());
                    studentComboBox.Items.Add(StdAIDreader["RegistrationNumber"].ToString());
                }
            }
            StdAIDreader.Close();

            var conR = Configuration.getInstance().getConnection();
            SqlCommand findRubricMID = new SqlCommand("Select Id,MeasurementLevel From RubricLevel", conR);
            SqlDataReader RMIDreader = findRubricMID.ExecuteReader();
            if (RMIDreader.HasRows)
            {
                while (RMIDreader.Read())
                {
                    rm.Add(RMIDreader["Id"].ToString(), RMIDreader["MeasurementLevel"].ToString());
                    rubricComboBox.Items.Add(RMIDreader["MeasurementLevel"].ToString());
                }
            }
            RMIDreader.Close();
        }

        private void addevaluationBtn_Click(object sender, EventArgs e)
        {
            try
            {

                int rubricMId = 0;
                int assessmenCId = 0;
                int studentId = 0;
                string selectedRubric = rubricComboBox.Text;
                string selectedAssessment = assessmentComboBox.Text;
                string selectedStudent = studentComboBox.Text;
                DateTime systemDate = DateTime.Now;
                DateTime currentDate = systemDate.Date;
                foreach (var a in ac)
                {
                    if (a.Value == selectedAssessment)
                    {
                        assessmenCId = int.Parse(a.Key);
                    }
                }

                foreach (var r in rm)
                {
                    if (r.Value == selectedRubric)
                    {
                        rubricMId = int.Parse(r.Key);
                    }
                }

                foreach (var r in std)
                {
                    if (r.Value == selectedStudent)
                    {
                        studentId = int.Parse(r.Key);
                    }
                }

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into StudentResult values  (@StudentId, @AssessmentComponentId, @RubricMeasurementId, @EvaluationDate)", con);
                cmd.Parameters.AddWithValue("@StudentId", studentId);
                cmd.Parameters.AddWithValue("@AssessmentComponentId", assessmenCId);
                cmd.Parameters.AddWithValue("@RubricMeasurementId", rubricMId);
                cmd.Parameters.AddWithValue("@EvaluationDate", currentDate);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
            catch (System.Data.SqlClient.SqlException exp)
            {
                MessageBox.Show("Same student can not evaluated double against same assessment component");
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                rubricComboBox.Text = " ";
                assessmentComboBox.Text = " ";
                studentComboBox.Text = " ";

            }
        }
    }
}
