﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class addAssessment : Form
    {
        public addAssessment()
        {
            InitializeComponent();
        }

        private void conTBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void addAssessmentBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string title = titleTBox.Text;
                DateTime systemDate = DateTime.Now;
                DateTime currentDate = systemDate.Date;
                int totalMarks = int.Parse(totalMarksTBox.Text);
                int totalWeightage = int.Parse(totalWeightageTBox.Text);

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Assessment values  (@Title, @DateCreated, @TotalMarks, @TotalWeightage)", con);
                cmd.Parameters.AddWithValue("@Title", title);
                cmd.Parameters.AddWithValue("@DateCreated", currentDate);
                cmd.Parameters.AddWithValue("@TotalMarks", totalMarks);
                cmd.Parameters.AddWithValue("@TotalWeightage", totalWeightage);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                this.Close();
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                titleTBox.Text = "";
                totalWeightageTBox.Text = "";
                totalMarksTBox.Text = "";
            }

        }
       
        private void addAssessment_Load(object sender, EventArgs e)
        {

        }
    }
}
