﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class addStudentAttendance : Form
    {
        public addStudentAttendance()
        {
            InitializeComponent();
        }
     
        private Dictionary<string, string> std = new Dictionary<string, string>();
        private Dictionary<string, string> atd = new Dictionary<string, string>();
        private void addStudentAttendance_Load(object sender, EventArgs e)
        {

            try
            {
                DateTime onlyDate;
                var conAtd = Configuration.getInstance().getConnection();
                SqlCommand findAttendanceID = new SqlCommand("Select Id,AttendanceDate From ClassAttendance", conAtd);
                SqlDataReader Atdreader = findAttendanceID.ExecuteReader();
                if (Atdreader.HasRows)
                {
                    while (Atdreader.Read())
                    {
                        atd.Add(Atdreader["Id"].ToString(), Atdreader["AttendanceDate"].ToString());
                        onlyDate = Convert.ToDateTime(Atdreader["AttendanceDate"].ToString());
                        attendanceDateBox.Items.Add(onlyDate);
                    }
                }
                Atdreader.Close();
                StudentStatusComboBox.Items.Add("Present");
                StudentStatusComboBox.Items.Add("Absent");
                StudentStatusComboBox.Items.Add("Leave");
                StudentStatusComboBox.Items.Add("Late");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void adStdAttendanceBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int attendanceId = 0;
                int studentId = 0;
                int attendanceStatus = 0;
                
                if (StudentStatusComboBox.Text == "Present")
                {
                    attendanceStatus = 1;
                }
                else if (StudentStatusComboBox.Text == "Absent")
                {
                    attendanceStatus = 2;
                }
                else if (StudentStatusComboBox.Text == "Leave")
                {
                    attendanceStatus = 3;
                }
                else if (StudentStatusComboBox.Text == "Late")
                {
                    attendanceStatus = 4;
                }
                DateTime withoutTimedate = Convert.ToDateTime(attendanceDateBox.Text);
                withoutTimedate = withoutTimedate.Date;
                string ontlDateWithotTime = withoutTimedate.ToString();
                foreach (var r in atd)
                {
                    if (r.Value == ontlDateWithotTime)
                    {

                        attendanceId = int.Parse(r.Key);
                    }
                }
                foreach (var s in std)
                {
                    if (s.Value == studentComboBox.Text)
                    {
                        studentId = int.Parse(s.Key);
                    }
                }
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values  (@AttendanceId, @StudentId, @AttendanceStatus)", con);
                cmd.Parameters.AddWithValue("@AttendanceId", attendanceId);
                cmd.Parameters.AddWithValue("@StudentId", studentId);
                cmd.Parameters.AddWithValue("@AttendanceStatus", attendanceStatus);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

        }

        private void attendanceDateBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void attendanceDateBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                string queryForReg = "Select S.Id, S.RegistrationNumber From Student S Where(S.Status = 5) And S.Id IN(Select Id From Student s Except(Select StudentId From StudentAttendance sa, ClassAttendance cl Where sa.AttendanceId = cl.Id And cl.AttendanceDate = '2020-09-12'))";
                var conStd = Configuration.getInstance().getConnection();
                SqlCommand findStduentID = new SqlCommand(queryForReg, conStd);
                SqlDataReader stdreader = findStduentID.ExecuteReader();
                if (stdreader.HasRows)
                {
                    while (stdreader.Read())
                    {
                        std.Add(stdreader["Id"].ToString(), stdreader["RegistrationNumber"].ToString());
                        string reg = stdreader["RegistrationNumber"].ToString();
                        studentComboBox.Items.Add(reg);
                    }
                }
                stdreader.Close();
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
    }
}
