﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class assessmentComponentList : Form
    {
        public assessmentComponentList()
        {
            InitializeComponent();
        }
        private Form1 f1 = new Form1();
        private string queryForAssessmentComponent = "Select Id, Name, TotalMarks, DateUpdated, DateCreated From AssessmentComponent";
        private void assessmentComponentList_Load(object sender, EventArgs e)
        {
            f1.loaDdataIntoGrid(assessmentComponentDataGrid, queryForAssessmentComponent);
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string selected_Id = assessmentComponentDataGrid.CurrentRow.Cells[0].Value.ToString();

                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand(("DELETE FROM AssessmentComponent WHERE Id = '" + selected_Id + "' ") + ("DELETE FROM StudentResult WHERE AssessmentComponentId = '" + selected_Id + "' "), con1);
                cmd1.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(assessmentComponentDataGrid, queryForAssessmentComponent); // This function call will refresh grid
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool allAttributeValid = true;
                string selected_id = assessmentComponentDataGrid.CurrentRow.Cells[0].Value.ToString();
                string selected_Name = assessmentComponentDataGrid.CurrentRow.Cells[1].Value.ToString();
                string selected_tMarks = assessmentComponentDataGrid.CurrentRow.Cells[2].Value.ToString();
                DateTime systemDate = DateTime.Now;
                DateTime currentDate = systemDate.Date;
                if (allAttributeValid)
                {
                    var con1 = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("UPDATE AssessmentComponent SET Name = @Name, TotalMarks = @TotalMarks, DateUpdated = @DateUpdated Where Id = '" + selected_id + "' ", con1);
                    cmd.Parameters.AddWithValue("@Name", selected_Name);
                    cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(selected_tMarks));
                    cmd.Parameters.AddWithValue("@DateUpdated", currentDate);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Updated");
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(assessmentComponentDataGrid, queryForAssessmentComponent); // This function call will refresh grid
            }
        }

        private void exportPdfbtn_Click(object sender, EventArgs e)
        {

            if (assessmentComponentDataGrid.Rows.Count > 0)
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.Filter = "PDF (*.pdf)|*.pdf";
                saveFile.FileName = "AssessmentComponent.pdf";
                bool isNotValid = false;

                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(saveFile.FileName))
                    {
                        try
                        {
                            File.Delete(saveFile.FileName);
                        }
                        catch (Exception exp)
                        {
                            isNotValid = true;
                            MessageBox.Show("Unable write in pdf" + exp.Message);
                        }
                    }
                    if (!isNotValid)
                    {
                        try
                        {
                            PdfPTable pTable = new PdfPTable(assessmentComponentDataGrid.Columns.Count);
                            pTable.DefaultCell.Padding = 8;
                            pTable.WidthPercentage = 100;
                            pTable.HorizontalAlignment = Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn column in assessmentComponentDataGrid.Columns)
                            {
                                PdfPCell pCell = new PdfPCell(new Phrase(column.HeaderText));
                                pTable.AddCell(pCell);
                            }

                            foreach (DataGridViewRow viewRow in assessmentComponentDataGrid.Rows)
                            {
                                foreach (DataGridViewCell dcell in viewRow.Cells)
                                {
                                    pTable.AddCell(dcell.Value.ToString());
                                }
                            }
                            Paragraph space = new Paragraph("     ");
                            iTextSharp.text.Font headerFont = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            Paragraph p = new Paragraph("All Assessment Components", headerFont);
                            p.Alignment = Element.ALIGN_CENTER;
                            using (FileStream fileStream = new FileStream(saveFile.FileName, FileMode.Create))
                            {
                                Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);
                                PdfWriter.GetInstance(document, fileStream);
                                document.Open();
                                document.Add(space);
                                document.Add(p);
                                document.Add(space);
                                document.Add(pTable);
                                document.Close();
                                fileStream.Close();
                            }
                            MessageBox.Show("Pdf Successfully saved");
                        }
                        catch (Exception exp)

                        {
                            MessageBox.Show("Error while exporting Data" + exp.Message);
                        }

                    }

                }

                }

                else

                {

                    MessageBox.Show("No Record Found", "Info");

                }

            }
        }
}
