﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class AddRubricLevel : Form
    {
        public AddRubricLevel()
        {
            InitializeComponent();
        }
        private Dictionary<string, string> Rubr = new Dictionary<string, string>();

        private void addRubricLevelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int rId = 0;
                string rIdSelected = rubricIDTBox.Text;
                string detail = detailsTBox.Text;
                int measurementLevel = int.Parse(measurementLevelTBox.Text);

                foreach (var r in Rubr)
                {
                    if (r.Value == rIdSelected)
                    {
                        rId = int.Parse(r.Key);
                    }
                }

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into RubricLevel values  (@RubricId, @Details, @MeasurementLevel)", con);
                cmd.Parameters.AddWithValue("@RubricId", rId);
                cmd.Parameters.AddWithValue("@Details", detail);
                cmd.Parameters.AddWithValue("@MeasurementLevel", measurementLevel);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                this.Close();
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                rubricIDTBox.Text = " ";
                detailsTBox.Text = " ";
                measurementLevelTBox.Text = " ";
            }
        }

        private void AddRubricLevel_Load(object sender, EventArgs e)
        {
            try
            {
                var conRubric = Configuration.getInstance().getConnection();
                SqlCommand findRubricID = new SqlCommand("Select Id,Details From Rubric", conRubric);
                SqlDataReader RIDreader = findRubricID.ExecuteReader();
                if (RIDreader.HasRows)
                {
                    while (RIDreader.Read())
                    {
                        Rubr.Add(RIDreader["Id"].ToString(), RIDreader["Details"].ToString());
                        rubricIDTBox.Items.Add(RIDreader["Details"].ToString());
                    }
                }
                RIDreader.Close();
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }



        }
    }
}
