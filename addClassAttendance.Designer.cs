﻿
namespace DB2020_CS_39
{
    partial class addClassAttendance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cloNameLabel = new System.Windows.Forms.Label();
            this.addCAttendanceBtn = new System.Windows.Forms.Button();
            this.getDate = new System.Windows.Forms.DateTimePicker();
            this.stdListHeadPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.stdListHeadPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.47183F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.52817F));
            this.tableLayoutPanel1.Controls.Add(this.cloNameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.addCAttendanceBtn, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.getDate, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(114, 132);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.11765F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.88235F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(455, 204);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // cloNameLabel
            // 
            this.cloNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cloNameLabel.AutoSize = true;
            this.cloNameLabel.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.cloNameLabel.Location = new System.Drawing.Point(3, 35);
            this.cloNameLabel.Name = "cloNameLabel";
            this.cloNameLabel.Size = new System.Drawing.Size(105, 20);
            this.cloNameLabel.TabIndex = 9;
            this.cloNameLabel.Text = "Get Date";
            // 
            // addCAttendanceBtn
            // 
            this.addCAttendanceBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.addCAttendanceBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.addCAttendanceBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addCAttendanceBtn.Location = new System.Drawing.Point(3, 93);
            this.addCAttendanceBtn.Name = "addCAttendanceBtn";
            this.addCAttendanceBtn.Size = new System.Drawing.Size(84, 38);
            this.addCAttendanceBtn.TabIndex = 16;
            this.addCAttendanceBtn.Text = "ADD";
            this.addCAttendanceBtn.UseVisualStyleBackColor = false;
            this.addCAttendanceBtn.Click += new System.EventHandler(this.addCAttendanceBtn_Click);
            // 
            // getDate
            // 
            this.getDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.getDate.Location = new System.Drawing.Point(114, 33);
            this.getDate.Name = "getDate";
            this.getDate.Size = new System.Drawing.Size(338, 23);
            this.getDate.TabIndex = 17;
            // 
            // stdListHeadPanel
            // 
            this.stdListHeadPanel.ColumnCount = 3;
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.7993F));
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.75524F));
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.44547F));
            this.stdListHeadPanel.Controls.Add(this.label1, 1, 0);
            this.stdListHeadPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.stdListHeadPanel.Location = new System.Drawing.Point(0, 0);
            this.stdListHeadPanel.Name = "stdListHeadPanel";
            this.stdListHeadPanel.RowCount = 1;
            this.stdListHeadPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.stdListHeadPanel.Size = new System.Drawing.Size(684, 87);
            this.stdListHeadPanel.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(172, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(334, 87);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add Attendance Date";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // addClassAttendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 475);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.stdListHeadPanel);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "addClassAttendance";
            this.Text = "addClassAttendance";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.stdListHeadPanel.ResumeLayout(false);
            this.stdListHeadPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label cloNameLabel;
        private System.Windows.Forms.Button addCAttendanceBtn;
        private System.Windows.Forms.TableLayoutPanel stdListHeadPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker getDate;
    }
}