﻿
namespace DB2020_CS_39
{
    partial class addClos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cloNameLabel = new System.Windows.Forms.Label();
            this.cloNameTBox = new System.Windows.Forms.TextBox();
            this.addCloBtn = new System.Windows.Forms.Button();
            this.stdListHeadPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.stdListHeadPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.47183F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.52817F));
            this.tableLayoutPanel1.Controls.Add(this.cloNameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cloNameTBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.addCloBtn, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(70, 125);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.11765F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.88235F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(568, 204);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // cloNameLabel
            // 
            this.cloNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cloNameLabel.AutoSize = true;
            this.cloNameLabel.Location = new System.Drawing.Point(3, 37);
            this.cloNameLabel.Name = "cloNameLabel";
            this.cloNameLabel.Size = new System.Drawing.Size(133, 16);
            this.cloNameLabel.TabIndex = 9;
            this.cloNameLabel.Text = "CLO Name";
            // 
            // cloNameTBox
            // 
            this.cloNameTBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cloNameTBox.Location = new System.Drawing.Point(142, 33);
            this.cloNameTBox.Name = "cloNameTBox";
            this.cloNameTBox.Size = new System.Drawing.Size(423, 23);
            this.cloNameTBox.TabIndex = 12;
            // 
            // addCloBtn
            // 
            this.addCloBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.addCloBtn.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.addCloBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.addCloBtn.Location = new System.Drawing.Point(3, 93);
            this.addCloBtn.Name = "addCloBtn";
            this.addCloBtn.Size = new System.Drawing.Size(84, 38);
            this.addCloBtn.TabIndex = 16;
            this.addCloBtn.Text = "ADD";
            this.addCloBtn.UseVisualStyleBackColor = false;
            this.addCloBtn.Click += new System.EventHandler(this.addStdBtn_Click);
            // 
            // stdListHeadPanel
            // 
            this.stdListHeadPanel.ColumnCount = 3;
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.stdListHeadPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.stdListHeadPanel.Controls.Add(this.label1, 1, 0);
            this.stdListHeadPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.stdListHeadPanel.Location = new System.Drawing.Point(0, 0);
            this.stdListHeadPanel.Name = "stdListHeadPanel";
            this.stdListHeadPanel.RowCount = 1;
            this.stdListHeadPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.stdListHeadPanel.Size = new System.Drawing.Size(684, 87);
            this.stdListHeadPanel.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(231, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 87);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add CLO";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // addClos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 475);
            this.Controls.Add(this.stdListHeadPanel);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "addClos";
            this.Text = "addClos";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.stdListHeadPanel.ResumeLayout(false);
            this.stdListHeadPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label cloNameLabel;
        private System.Windows.Forms.TextBox cloNameTBox;
        private System.Windows.Forms.Button addCloBtn;
        private System.Windows.Forms.TableLayoutPanel stdListHeadPanel;
        private System.Windows.Forms.Label label1;
    }
}