﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class rubricList : Form
    {
        public rubricList()
        {
            InitializeComponent();
        }
        private Form1 f1 = new Form1();
        private string queryForRubric = "Select Id, Details From Rubric";
        private void rubricList_Load(object sender, EventArgs e)
        {
            f1.loaDdataIntoGrid(rubricListDataGrid, queryForRubric);
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedRId = 0;
                List<int> acId = new List<int>();
                string forConversion = "";
                string selected_Id = rubricListDataGrid.CurrentRow.Cells[0].Value.ToString();
                string findAciDQuery = "Select Id From RubricLevel Where RubricId  Like '" + selected_Id + "' ";
                var conAc = Configuration.getInstance().getConnection();
                SqlCommand findAcID = new SqlCommand(findAciDQuery, conAc);
                SqlDataReader RacIdreader = findAcID.ExecuteReader();
                if (RacIdreader.HasRows)
                {
                    while (RacIdreader.Read())
                    {
                        forConversion = RacIdreader["Id"].ToString();
                        selectedRId = int.Parse(forConversion);
                        acId.Add(selectedRId);
                    }
                }
                RacIdreader.Close();

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(("DELETE FROM AssessmentComponent WHERE RubricId = '" + selected_Id + "' ") + ("DELETE FROM Rubric WHERE Id = '" + selected_Id + "' ")+("DELETE FROM RubricLevel WHERE RubricId = '" + selected_Id + "' "), con);
                cmd.ExecuteNonQuery();
                foreach (var i in acId)
                {
                    var con2 = Configuration.getInstance().getConnection();
                    SqlCommand cmd2 = new SqlCommand(("DELETE FROM StudentResult WHERE RubricMeasurementId  = '" + i + "' "), con2);
                    cmd2.ExecuteNonQuery();
                }
                MessageBox.Show("Successfully Deleted");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(rubricListDataGrid, queryForRubric); // This function call will refresh grid
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string selected_Id = rubricListDataGrid.CurrentRow.Cells[0].Value.ToString();
                string selected_Detail = rubricListDataGrid.CurrentRow.Cells[1].Value.ToString();

                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE Rubric SET Details = @Details Where Id = '" + selected_Id + "' ", con1);
                cmd.Parameters.AddWithValue("@Details", selected_Detail);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(rubricListDataGrid, queryForRubric); // This function call will refresh grid
            }
        }

        private void esportPdf_Click(object sender, EventArgs e)
        {
            if (rubricListDataGrid.Rows.Count > 0)
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.Filter = "PDF (*.pdf)|*.pdf";
                saveFile.FileName = "Rubrics.pdf";
                bool isNotValid = false;

                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(saveFile.FileName))
                    {
                        try
                        {
                            File.Delete(saveFile.FileName);
                        }
                        catch (Exception exp)
                        {
                            isNotValid = true;
                            MessageBox.Show("Unable write in pdf" + exp.Message);
                        }
                    }
                    if (!isNotValid)
                    {
                        try
                        {
                            PdfPTable pTable = new PdfPTable(rubricListDataGrid.Columns.Count);
                            pTable.DefaultCell.Padding = 8;
                            pTable.WidthPercentage = 100;
                            pTable.HorizontalAlignment = Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn column in rubricListDataGrid.Columns)
                            {
                                PdfPCell pCell = new PdfPCell(new Phrase(column.HeaderText));
                                pTable.AddCell(pCell);
                            }

                            foreach (DataGridViewRow viewRow in rubricListDataGrid.Rows)
                            {
                                foreach (DataGridViewCell dcell in viewRow.Cells)
                                {
                                    pTable.AddCell(dcell.Value.ToString());
                                }
                            }
                            Paragraph space = new Paragraph("     ");
                            iTextSharp.text.Font headerFont = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            Paragraph p = new Paragraph("All Rubrics", headerFont);
                            p.Alignment = Element.ALIGN_CENTER;
                            using (FileStream fileStream = new FileStream(saveFile.FileName, FileMode.Create))
                            {
                                Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);
                                PdfWriter.GetInstance(document, fileStream);
                                document.Open();
                                document.Add(space);
                                document.Add(p);
                                document.Add(space);
                                document.Add(pTable);
                                document.Close();
                                fileStream.Close();
                            }
                            MessageBox.Show("Pdf Successfully saved");
                        }
                        catch (Exception exp)

                        {
                            MessageBox.Show("Error while exporting Data" + exp.Message);
                        }

                    }

                }

            }

            else

            {

                MessageBox.Show("No Data Found");

            }
        }
    }
}
