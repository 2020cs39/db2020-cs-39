﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_39
{
    public partial class attendanceList : Form
    {
        public attendanceList()
        {
            InitializeComponent();
        }
        private Form1 f1 = new Form1();
        private string queryForAttendance = "Select S.RegistrationNumber As [Registration Number],  " +
                "(Case SA.AttendanceStatus When 3 then 'Leave' When 2 then 'Absent' When 1 then 'Present' When 4 then 'Late' end) As[Attendance Status], " +
                "CA.AttendanceDate As[Attendance Date] From StudentAttendance As SA " +
                "Join Student As S On S.Id = SA.StudentId Join ClassAttendance As CA On CA.Id = SA.AttendanceId";

        private void attendanceList_Load(object sender, EventArgs e)
        {
            f1.loaDdataIntoGrid(attendanceDataGrid, queryForAttendance);
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int selected_Id = 0;
                string forConversion = " ";
                string selected_reg = attendanceDataGrid.CurrentRow.Cells[0].Value.ToString();
                string findStdQuery = "Select Id From Student Where RegistrationNumber Like '" + selected_reg + "' ";
                var conRubric = Configuration.getInstance().getConnection();
                SqlCommand findRubricID = new SqlCommand(findStdQuery, conRubric);
                SqlDataReader RIDreader = findRubricID.ExecuteReader();
                if (RIDreader.HasRows)
                {
                    while (RIDreader.Read())
                    {
                        forConversion = RIDreader["Id"].ToString();
                        selected_Id = int.Parse(forConversion);
                    }
                }
                RIDreader.Close();

                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand(("DELETE FROM StudentAttendance WHERE StudentId = '" + selected_Id + "' "), con1);
                cmd1.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(attendanceDataGrid, queryForAttendance); // This function call will refresh grid
            }

        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int upStatus = 0;
                string forConversion = "";
                bool allAttributeValid = true;
                string selected_reg = attendanceDataGrid.CurrentRow.Cells[0].Value.ToString();
                string selected_status = attendanceDataGrid.CurrentRow.Cells[1].Value.ToString();
                int selected_Id = 0;
                string findStdQuery = "Select Id,Email From Student Where RegistrationNumber Like '" + selected_reg + "' ";
                var conRubric = Configuration.getInstance().getConnection();
                SqlCommand findRubricID = new SqlCommand(findStdQuery, conRubric);
                SqlDataReader RIDreader = findRubricID.ExecuteReader();
                if (RIDreader.HasRows)
                {
                    while (RIDreader.Read())
                    {
                        forConversion = RIDreader["Id"].ToString();
                        selected_Id = int.Parse(forConversion);
                    }
                }
                RIDreader.Close();

                if (selected_status == "Present")
                {
                    upStatus = 1;
                }
                else if (selected_status == "Absent")
                {
                    upStatus = 2;
                }
                else if (selected_status == "Leave")
                {
                    upStatus = 3;
                }
                else if (selected_status == "Late")
                {
                    upStatus = 4;
                }
                else
                {
                    allAttributeValid = false;
                    MessageBox.Show("Enter Status in Right Format");
                }
                if (allAttributeValid)
                {
                    var con1 = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("UPDATE StudentAttendance SET AttendanceStatus =  @AttendanceStatus Where StudentId = '" + selected_Id + "' ", con1);
                    cmd.Parameters.AddWithValue("@AttendanceStatus", upStatus);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Updated");
                }

            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                f1.loaDdataIntoGrid(attendanceDataGrid, queryForAttendance); // This function call will refresh grid
            }
        }

        private void exportPDF_Click(object sender, EventArgs e)
        {
            if (attendanceDataGrid.Rows.Count > 0)
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.Filter = "PDF (*.pdf)|*.pdf";
                saveFile.FileName = "Attendace.pdf";
                bool isNotValid = false;

                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(saveFile.FileName))
                    {
                        try
                        {
                            File.Delete(saveFile.FileName);
                        }
                        catch (Exception exp)
                        {
                            isNotValid = true;
                            MessageBox.Show("Unable write in pdf" + exp.Message);
                        }
                    }
                    if (!isNotValid)
                    {
                        try
                        {
                            PdfPTable pTable = new PdfPTable(attendanceDataGrid.Columns.Count);
                            pTable.DefaultCell.Padding = 8;
                            pTable.WidthPercentage = 100;
                            pTable.HorizontalAlignment = Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn column in attendanceDataGrid.Columns)
                            {
                                PdfPCell pCell = new PdfPCell(new Phrase(column.HeaderText));
                                pTable.AddCell(pCell);
                            }

                            foreach (DataGridViewRow viewRow in attendanceDataGrid.Rows)
                            {
                                foreach (DataGridViewCell dcell in viewRow.Cells)
                                {
                                    pTable.AddCell(dcell.Value.ToString());
                                }
                            }
                            Paragraph space = new Paragraph("     ");
                            iTextSharp.text.Font headerFont = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            Paragraph p = new Paragraph("Attendance Of Students", headerFont);
                            p.Alignment = Element.ALIGN_CENTER;
                            using (FileStream fileStream = new FileStream(saveFile.FileName, FileMode.Create))
                            {
                                Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);
                                PdfWriter.GetInstance(document, fileStream);
                                document.Open();
                                document.Add(space);
                                document.Add(p);
                                document.Add(space);
                                document.Add(pTable);
                                document.Close();
                                fileStream.Close();
                            }
                            MessageBox.Show("Pdf Successfully saved");
                        }
                        catch (Exception exp)

                        {
                            MessageBox.Show("Error while exporting Data" + exp.Message);
                        }

                    }

                }

            }

            else

            {

                MessageBox.Show("No Record Found", "Info");

            }
        }
    }
}
